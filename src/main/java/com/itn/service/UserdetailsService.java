/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.service;

import com.itn.dao.UserdetailsDao;
import com.itn.entity.Userdetails;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rebel
 */
@Service
public class UserdetailsService implements UserdetailsServiceInterface {
@Autowired
    UserdetailsDao udao;

    public UserdetailsDao getUdao() {
        return udao;
    }

    public void setUdao(UserdetailsDao udao) {
        this.udao = udao;
    }

    @Transactional
    @Override
    public void insert(Userdetails u) {
    udao.insert(u);   
    }

    @Override
    public List<Userdetails> display() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int uid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Userdetails display_by_id(int uid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Userdetails u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
