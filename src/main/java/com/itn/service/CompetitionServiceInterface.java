/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.service;

import com.itn.entity.Competition;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 *
 * @author Rebel
 */
@Service
public interface CompetitionServiceInterface {
    public void insert(Competition c);
    public List<Competition>display();
    public void delete(int cid);
    public Competition display_by_id(int cid);
    public void update(Competition c); 
    
}
