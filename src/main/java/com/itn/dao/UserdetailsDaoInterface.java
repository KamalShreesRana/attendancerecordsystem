/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.dao;

import com.itn.entity.Userdetails;
import java.util.List;

/**
 *
 * @author Rebel
 */
public interface UserdetailsDaoInterface {
     public void insert(Userdetails u);
    public List<Userdetails>display();
    public void delete(int uid);
    public Userdetails display_by_id(int uid);
    public void update(Userdetails u); 
}
