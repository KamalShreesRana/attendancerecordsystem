<%@page buffer="16kb" autoFlush="true" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="HeaderProperties.jsp" %>
</head>

<body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        
        <%@include file="TopNavBar.jsp" %>
        
        <!-- main / large navbar -->
        

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Edit Seminar</h1>
                            </div>
                        </div>
                    </div>   
                        
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <span>Student</span>
                                    </li>
                                    <li class="active">Edit Student</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Student's Form</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                    <form action="SubmitEditStudent" method="POST" id="validateform" class="form-horizontal">
                                        <fieldset>
                                            <legend>Edit Student's Information</legend>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Sid </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="sid" value="${seminar.sid}" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Student Name </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarname" value="${seminar.seminarname}" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Entry Date </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminardate" value="${seminar.seminardate}" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Level </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarduration" value="${seminar.seminarduration}" class="form-control" >
                                                                                      </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Email </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarbuilding" value="${seminar.seminarbuilding}" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Attendance </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarmaster" value="${seminar.seminarmaster}" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        
                                        </fieldset>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>

<!--    <script>
    $(document).ready(function() {
    $('#validateform').formValidation({
        framework: 'bootstrap',
      
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            boarding: {
                validators: {
                    notEmpty: {
                        message: 'The boarding is required and cannot be empty'
                    }
                }
            },
            destination: {
                validators: {
                    notEmpty: {
                        message: 'The destination is required and cannot be empty'
                    },
                    
                }
            },
            distance: {
                validators: {
                    notEmpty: {
                        message: 'The distance is required and cannot be empty'
                    },
                }
            },
            time: {
                validators: {
                    notEmpty: {
                        message: 'The time is required and cannot be empty'
                    },
                }
            },
            
        }
    });
});
</script>-->
        
</body>

</html>
