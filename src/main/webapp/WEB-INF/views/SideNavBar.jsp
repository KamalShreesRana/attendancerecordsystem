<div class="col-md-2 bootstrap-admin-col-left">
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                        
                        
                        <li class="active">
                            <a href="Dashboard" style="background: #17491C;"><i class="glyphicon glyphicon-home"></i> Dashboard</a>
                        </li>
                    
                    </ul>
                    
                    <br/>
                    
                    
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side" >

                        <li class="active" >
                            <a href="" style="background: #17491C;"><i class="glyphicon glyphicon-user"></i> College Staff Attendance Record </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                                <li><a href="AddStaffAttendance"><i class="glyphicon glyphicon-chevron-right"></i> Add Staff Attendance Record </a></li>
                                <li><a href="DisplayStaffRecord"><i class="glyphicon glyphicon-chevron-right"></i> Display Staff Attendance Record </a></li>
                            </ul>
                        </li>

                    </ul>
                    
                    <br/>
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">

                        <li class="active">
                            <a href="" style="background: #17491C;"><i class="glyphicon glyphicon-user"></i> College Student Attendance Record </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                               <li><a href="AddStudentAttendance"><i class="glyphicon glyphicon-chevron-right"></i> Add Student Attendance Record </a></li>
                               <li><a href="DisplayStudentRecord"><i class="glyphicon glyphicon-chevron-right"></i> Display Student Attendance Record </a></li>
                            </ul>
                        </li>

                    </ul>
                    
               
                     <br/>
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">

                        <li class="active">
                            <a href="" style="background: #17491C;"><i class="glyphicon glyphicon-user"></i> Manage User </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                                <li><a href="AddUser"><i class="glyphicon glyphicon-chevron-right"></i> Register User </a></li>
                                
                            </ul>
                        </li>

                    </ul>
                    
                    
                </div>