<%@page buffer="16kb" autoFlush="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <%@include file="HeaderProperties.jsp" %>

    </head>

    <body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->

        <%@include file="TopNavBar.jsp" %>

        <!-- main / large navbar -->


        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Add User</h1>
                            </div>
                        </div>
                    </div>   

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <span>User</span>
                                    </li>
                                    <li class="active">Add User</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="span9" id="content">



                            <div class="row-fluid">
                                <!-- block -->
                                <div class="block">
                                   
                                    <div class="block-content collapse in">
                                        <div class="span12">
                                            <form class="form-horizontal" action="SubmitAddUser" method="POST">
                                                
                                                    <div class="control-group">
                                                        <label class="control-label" for="typeahead">Enter User Name</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control"  id="typeahead"  name="username" >
                                                            
                                                        </div>
                                                    </div>
                                                
                                                    <div class="control-group">
                                                        <label class="control-label" for="typeahead">Type Password</label>
                                                        <div class="controls">
                                                            <input type="password" class="form-control"  id="typeahead"  name="password">
                                                            
                                                        </div>
                                                    </div>  
                                                
                                                                                                         
                                                    <div class="control-group">
                                                        <label class="control-label" for="select01">Select Role</label>
                                                        <div class="controls">
                                                            <select id="select01" class="form-control"  name="role">
                                                                <option>Select User Role</option>
                                                                <option>ROLE_ADMIN</option>
                                                                <option>ROLE_USER</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>                                                
                                                                                                         
                                                    <div class="control-group">
                                                        <label class="control-label" for="select01">Select Status</label>
                                                        <div class="controls">
                                                            <select id="select01" class="form-control"  name="enabled">
                                                                <option>Select User Status</option>
                                                                <option value="1">Active</option>
                                                                <option value="2">Inactive</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                        <button type="reset" class="btn">Cancel</button>
                                                    </div>
                                             
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!-- /block -->
                            </div>



                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>

        <script>
            $(document).ready(function () {
                $('#validateform').formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        seminarname: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar name is required and cannot be empty'
                                }
                            }
                        },
                        seminardate: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar name date is required and cannot be empty'
                                },
                            }
                        },
                        seminarduration: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar duration is required and cannot be empty'
                                },
                            }
                        },
                        seminarbuilding: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar building is required and cannot be empty'
                                },
                            }
                        },
                        seminarfee: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar fee is required and cannot be empty'
                                },
                            }
                        },
                        seminarinstructor: {
                            validators: {
                                notEmpty: {
                                    message: 'The seminar instructor is required and cannot be empty'
                                },
                            }
                        },
                    }
                });
            });
        </script>

    </body>

</html>
