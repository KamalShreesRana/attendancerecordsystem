package com.itn.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-06T14:37:28")
@StaticMetamodel(Userdetails.class)
public class Userdetails_ { 

    public static volatile SingularAttribute<Userdetails, Integer> uid;
    public static volatile SingularAttribute<Userdetails, Integer> enabled;
    public static volatile SingularAttribute<Userdetails, String> username;
    public static volatile SingularAttribute<Userdetails, String> role;
    public static volatile SingularAttribute<Userdetails, String> password;

}