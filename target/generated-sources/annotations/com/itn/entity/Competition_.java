package com.itn.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-06T14:37:28")
@StaticMetamodel(Competition.class)
public class Competition_ { 

    public static volatile SingularAttribute<Competition, String> competitiondate;
    public static volatile SingularAttribute<Competition, String> competitionbuilding;
    public static volatile SingularAttribute<Competition, String> competitionname;
    public static volatile SingularAttribute<Competition, String> competitionduration;
    public static volatile SingularAttribute<Competition, String> competitionmaster;
    public static volatile SingularAttribute<Competition, Integer> cid;

}