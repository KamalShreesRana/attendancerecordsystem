<!-- small navbar -->
    
    <!-- main / large navbar -->
    <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar " role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a style="float:right !important; color: #000; padding-top: 10px;" class="text-right" href="<c:url value="j_spring_security_logout" />">
                           Logout</a>
                    </div>
                    
       
                    
                    <div class="collapse navbar-collapse main-navbar-collapse">             
                        
                        <ul class="nav navbar-nav">
                          
                            
                            
                            
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div>
        </div><!-- /.container -->
    </nav>