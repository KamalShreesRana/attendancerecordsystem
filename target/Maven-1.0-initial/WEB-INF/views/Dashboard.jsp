
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
<head>
    <title>Attendance Record System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/easypiechart/jquery.easy-pie-chart.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/easypiechart/jquery.easy-pie-chart_custom.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script type="text/javascript" src="js/html5shiv.js"></script>
           <script type="text/javascript" src="js/respond.min.js"></script>
        <![endif]-->

</head>
   
<body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        <%@include file="TopNavBar.jsp" %>
        <!-- main / large navbar -->
       

        <div class="container">
            <!-- left, vertical navbar & content -->
             
           <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>
                
                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Admin Dashboard</h1>
                                
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="Dashboard">Admin Dashboard</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-5 staff" style="background: #6FA875; padding: 10px;">
                                <h2><center>STAFF ATTENDANCE RECORD</center></h2>
                                <p><center><a href="DisplayCompetition" style="color: #fff;">View Record</a></center></p>
                            </div>

                            <div class="col-md-2"></div>
                            
                            <div class="col-md-5 student" style="background: #6FA875; padding: 10px; ">
                                <h2><center>STUDENT ATTENDANCE RECORD</center></h2>
                                <p><center><a href="DisplayCompetition" style="color: #fff;">View Record</a></center></p>
                            </div>

                            <div class="col-lg-12"></div>
                            
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="assets/adminassets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/adminassets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="assets/adminassets/js/bootstrap-admin-theme-change-size.js"></script>
        <script type="text/javascript" src="assets/adminassets/vendors/easypiechart/jquery.easy-pie-chart.js"></script>

        <script type="text/javascript">
            $(function() {
                // Easy pie charts
                $('.easyPieChart').easyPieChart({animate: 1000});
            });
        </script>
        
        
    </body>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
		Welcome : ${pageContext.request.userPrincipal.name} | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a></h2>  
    </c:if>
    
</html>
