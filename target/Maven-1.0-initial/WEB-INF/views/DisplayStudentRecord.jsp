<%@page buffer="16kb" autoFlush="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="HeaderProperties.jsp" %>
    </head>
    <body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        <%@include file="TopNavBar.jsp" %>
        <!-- main / large navbar -->
        

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>
                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Display Student Record</h1>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="Dashboard">Home</a>
                                    </li>
                                    <li>
                                        <span>Record</span>
                                    </li>
                                    <li class="active">Display Record</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Display Student Record</div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                    <table class="table table-striped table-bordered" id="example">
                                        <thead>
                                            <tr>                                              
                                                <th>Student Name</th>
                                                <th>Entry Date</th>
                                                <th>Level</th>
                                                <th>Email</th>
                                                <th>Attendance</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            
                                            <c:forEach items="${seminar}" var="seminar">
                                            
                                            <tr >
                                                <td>${seminar.seminarname}</td>
                                                <td>${seminar.seminardate}</td>
                                                <td>${seminar.seminarduration}</td>
                                                <td>${seminar.seminarbuilding}</td>
                                                <td>${seminar.seminarmaster}</td>
                                                
                                                <td class="actions">
                                                    
                                                    <a href="AddStudentAttendance">
                                                        <button class="btn btn-xs btn-success">
                                                            <i class="glyphicon glyphicon-plus"></i>
                                                            Add
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="EditStudent?sid=${seminar.sid}&action=edit">
                                                        <button class="btn btn-xs btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="DeleteSeminar?sid=${seminar.sid}&action=delete">
                                                        <button class="btn btn-xs btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                    
                                                
                                                </td>
                                            </tr>
                                           
                                            </c:forEach>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>

    </body>
</html>

