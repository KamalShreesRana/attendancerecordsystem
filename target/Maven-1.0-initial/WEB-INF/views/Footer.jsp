<!-- footer -->
<div class="navbar navbar-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <footer role="contentinfo">

                    <p class="text-center"> &nbsp; Attendance Record System</p>
           
                </footer>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/adminassets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/adminassets/js/bootstrap-admin-theme-change-size.js"></script>
<script type="text/javascript" src="assets/vendors/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="assets/vendors/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/vendors/selectize/dist/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/javascripts/bootstrap-wysihtml5/wysihtml5.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/javascripts/bootstrap-wysihtml5/core-b3.js"></script>
<script type="text/javascript" src="assets/vendors/twitter-bootstrap-wizard/jquery.bootstrap.wizard-for.bootstrap3.js"></script>
<script type="text/javascript" src="assets/vendors/boostrap3-typeahead/bootstrap3-typeahead.min.js"></script>