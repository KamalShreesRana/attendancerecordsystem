<title>Attendance Record System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/bootstrap-datepicker/css/datepicker.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/datepicker.fixes.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/uniform/themes/default/css/uniform.default.min.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/uniform.default.fixes.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/chosen.min.css">
        <link rel="stylesheet" media="screen" href="assets//adminassets/vendors/selectize/dist/css/selectize.bootstrap3.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/stylesheets/bootstrap-wysihtml5/core-b3.css">

        <script type="text/javascript" src="assets/adminassets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/adminassets/js/jquery-1.12.4.min.js"></script> 
        <link rel="stylesheet" href="assets/adminassets/vendor/formvalidation/css/formValidation.min.css">
  
        <!-- FormValidation plugin and the class supports validating Bootstrap form -->
        <script src="assets/adminassets/vendor/formvalidation/js/formValidation.min.js"></script>
        <script src="assets/adminassets/vendor/formvalidation/js/framework/bootstrap.min.js"></script>