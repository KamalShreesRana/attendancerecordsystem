<%@page buffer="16kb" autoFlush="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    
    <%@include file="HeaderProperties.jsp" %>
    
</head>

<body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        <%@include file="TopNavBar.jsp" %>
        
        <!-- main / large navbar -->
        

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Add Student Attendance Record</h1>
                            </div>
                        </div>
                    </div>   
                        
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="navbar navbar-default bootstrap-admin-navbar-thin">
                                <ol class="breadcrumb bootstrap-admin-breadcrumb">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <span>Form</span>
                                    </li>
                                    <li class="active">Add Record</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Student Record Form</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                    <form action="SubmitAddStudentAttendance" method="POST" id="validateform" class="form-horizontal">
                                        <fieldset>
                                            <legend>Add Student Details</legend>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Student Name </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarname" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Entry Date </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminardate" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Level </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarduration" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Enail </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarbuilding" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label"> Attendance </label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="seminarmaster" class="form-control" >
                                                </div>
                                            </div>                                            
                                            
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        
                                        </fieldset>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>
        
    <script>
    $(document).ready(function() {
    $('#validateform').formValidation({
        framework: 'bootstrap',
      
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            boarding: {
                validators: {
                    notEmpty: {
                        message: 'The boarding is required and cannot be empty'
                    }
                }
            },
            eventname: {
                validators: {
                    notEmpty: {
                        message: 'The event name is required and cannot be empty'
                    },
                    
                }
            },
            eventtime: {
                validators: {
                    notEmpty: {
                        message: 'The event time is required and cannot be empty'
                    },
                }
            },
            eventduration: {
                validators: {
                    notEmpty: {
                        message: 'The event duration is required and cannot be empty'
                    },
                }
            },
            
        }
    });
});
</script>
        
</body>

</html>
