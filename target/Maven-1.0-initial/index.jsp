<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Attendance Record System</title>
    <link href="assets/lassets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/lassets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/lassets/css/main.css" rel="stylesheet">
    <link href="assets/lassets/css/animate.css" rel="stylesheet">	
    <link href="assets/lassets/css/responsive.css" rel="stylesheet">       
    <link rel="shortcut icon" href="assets/lassets/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/lassets/images/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/lassets/images/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/lassets/images/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/lassets/images/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav " >
			<div class="container">
				    
		        <div class="row col-md-12" style="background: none;" >	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.html" style="background: none;">
                                    <span><h3 style="font-weight: bold; color:#000; padding-left: 100px; ">ATTANDANCE</h3><span style=" color:#000; padding-left: 120px; ">Record</span></span>
		                </a>                    
		            </div>
		            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right" style="background: none;">                 
		                    <li class="scroll active" ><a href="#home" style="color: #000;">Home</a></li> 
		                    <li class="scroll"><a href="#about" style="color: #000;">About</a></li>       
		                    <li><a class="no-scroll" href="Dashboard" target="_blank" style="color: #000;">Admin Login</a></li>
		                    <li class="scroll"><a href="#contact" style="color: #000;">Contact</a></li>       
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
                                    <img class="img-responsive" src="assets/lassets/images/slider/bg1.jpg" alt="slider" style="max-height: 550px;">						
					<div class="carousel-caption" style="margin-left: 500px;">
						<h2>ATTEND EVERY DAY</h2>
                                            </div>
				</div>
				<div class="item">
					<img class="img-responsive" src="assets/lassets/images/slider/bg2.jpg" alt="slider" style="max-height: 550px;">	
					<div class="carousel-caption" style="margin-left: 500px;">
						<h2>PROGRESS EVERY DAY</h2>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="assets/lassets/images/slider/bg3.jpg" alt="slider" style="max-height: 550px;">	
					<div class="carousel-caption" style="margin-left: 500px;">
						<h2>ACHIEVE YOUR GOAL</h2>
					</div>
				</div>				
			</div>
		</div>    	
    </section>
	<!--/#home-->

	<section id="about">
           
		<div class="about-content">					
					<h2>About Attendance Record System</h2>
					<p>What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
				
		</div>
             <div class="guitar2" style="background: #ADE5B3;">				
                 <img class="img-responsive" src="assets/lassets/images/slider/b5.jpg" width="100%;" style="margin-top: 0px;">
		</div>
	</section><!--/#about-->

	<section id="contact">
		<div class="contact-section" style="background: rgb(240, 248, 248);">
			<div class="ear-piece">
                            <img class="img-responsive" src="assets/lassets/images/slider/b4.jpg" width="100%;" style="max-height: 300px; padding-top: 20px; padding-left: 20px; " alt="">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-sm-offset-4">
						<div class="contact-text">
							<h3>Contact</h3>
							<address>
								E-mail: promo@party.com<br>
								Phone: +1 (123) 456 7890<br>
								Fax: +1 (123) 456 7891
							</address>
						</div>
						
					</div>
					<div class="col-sm-5">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.817213404246!2d85.31714151506175!3d27.69204378279825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19b19295555f%3A0xabfe5f4b310f97de!2sThe+British+College%2C+Kathmandu!5e0!3m2!1sen!2snp!4v1525700103665"
                                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>	
					</div>
				</div>
			</div>
		</div>		
	</section>
    <!--/#contact-->

    <footer id="footer" style="background: #17491C;">
        <div class="container">
            <div class="text-center">
                <p> Copyright  &copy;2018<a target="_blank" href="#"> Attendance Record System </a> All Rights Reserved.</p>                
            </div>
        </div>
    </footer>
    <!--/#footer-->
  
    <script type="text/javascript" src="assets/lassets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/lassets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="assets/lassets/js/gmaps.js"></script>
	<script type="text/javascript" src="assets/lassets/js/smoothscroll.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/lassets/js/coundown-timer.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.nav.js"></script>
    <script type="text/javascript" src="assets/lassets/js/main.js"></script>  
</body>

</html>
